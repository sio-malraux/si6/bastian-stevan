#include <libpq-fe.h>
#include <iostream>
#include <iomanip>
#include <cstring>

#include "affichage.h"

void affichage_information_connexion(PGconn *conn)
{
  std::string password;
  std::string slt;
  int ssl = PQsslInUse(conn);
  password = PQpass(conn);
  int calcul_mdp = std::strlen(PQpass(conn));

  for(int i = 0; i < calcul_mdp; i++)
  {
    password.at(i) = '*';
  }

  if(ssl == 1)
  {
    slt = "true";
  }
  else
  {
    slt = "false";
  }

  std::cout << " * utilisateur : " << PQuser(conn) << std::endl;
  std::cout << " * mot de passe : " << password << std::endl;
  std::cout << " * base de donnees : " << PQdb(conn) << std::endl;
  std::cout << " * port TCP : " << PQport(conn) << std::endl;
  std::cout << " * chiffrement SSL : " << slt << std::endl;
  std::cout << " * encodage : " << PQparameterStatus(conn, "client_encoding") << std::endl;
  std::cout << " * version du protocole : " << PQprotocolVersion(conn) << std::endl;
  std::cout << " * version du serveur : " << PQserverVersion(conn) << std::endl;
  std::cout << " * version de la bibliotheque 'libpq' du client : " << PQlibVersion() << std::endl;
  std::cout << std::endl;
}



void affichage(PGconn *conn, PGresult *resultat_req, const char *separateur, int longueur_champ_plus_long)
{
  int nombre_lignes = PQntuples(resultat_req);
  affichage_ligne(resultat_req, longueur_champ_plus_long);
  affichage_entete(resultat_req, separateur, longueur_champ_plus_long);
  affichage_ligne(resultat_req, longueur_champ_plus_long);
  affichage_resultat_requete(resultat_req, separateur, longueur_champ_plus_long);
  affichage_ligne(resultat_req, longueur_champ_plus_long);
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << "L'exécution de la requête SQL a retourné " << nombre_lignes << " enregistrements." << std::endl;
}



void affichage_information_sur_requete(PGconn *conn, PGresult *resultat_req)
{
  if(PQresultStatus(resultat_req) != PGRES_TUPLES_OK)
  {
    std::cerr << PQerrorMessage(conn);
    std::cerr << PQresStatus(PQresultStatus(resultat_req)) << std::endl;
  }
}



void affichage_ligne(PGresult *resultat_req, int longueur_champ_plus_long)
{
  int nombre_champs = PQnfields(resultat_req);
  std::cout << " ";

  for(int i = 0; i < (longueur_champ_plus_long * nombre_champs + (nombre_champs * 3 + 1)); i++)
  {
    std::cout << "-";
  }
}



void affichage_entete(PGresult *resultat_req, const char *separateur, int longueur_champ_plus_long)
{
  int nombre_champs = PQnfields(resultat_req);
  std::cout << std::endl;

  for(int compteur_champs = 0; compteur_champs < nombre_champs; compteur_champs++)
  {
    std::cout << separateur
              << std::left
              << std::setw(longueur_champ_plus_long)
              << PQfname(resultat_req, compteur_champs);
  }

  std::cout << separateur << std::endl;
}



void affichage_resultat_requete(PGresult *resultat_req, const char *separateur, int longueur_champ_plus_long)
{
  int nombre_lignes = PQntuples(resultat_req);
  int nombre_champs = PQnfields(resultat_req);
  std::cout << std::endl;

  for(int compteur_ligne = 0; compteur_ligne < nombre_lignes; compteur_ligne++)
  {
    for(int compteur_champ = 0; compteur_champ < nombre_champs; compteur_champ++)
    {
      int longueur_champ_reponce = std::strlen(PQgetvalue(resultat_req, compteur_ligne, compteur_champ));
      char *champs = PQgetvalue(resultat_req, compteur_ligne, compteur_champ);

      if(longueur_champ_reponce > longueur_champ_plus_long)
      {
        champs[longueur_champ_plus_long] = '\0';

        for(int j = longueur_champ_plus_long - 3; j < longueur_champ_plus_long; j++)
        {
          champs[j] = '.';
        }
      }

      std::cout << separateur
                << std::left
                << std::setw(longueur_champ_plus_long)
                << PQgetvalue(resultat_req, compteur_ligne, compteur_champ);
    }

    std::cout << separateur << std::endl;
  }
}
