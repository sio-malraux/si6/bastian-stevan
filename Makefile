# Fichier Makefile
# Toutes les commandes doivent précédés d'une tabulation.

# Definition des variables

CXX = g++
AR = ar
CXXFLAGS = -ggdb -O0 -Werror -Wall  $(shell pkg-config --cflags libpq) -fPIC
LDFLAGS = $(shell pkg-config --libs libpq)

SOURCES = main.cpp
LIB_SOURCES = affichage.cpp calculatoire.cpp

# Voir
# https://www.gnu.org/software/make/manual/make.html#Text-Functions
OBJS = $(patsubst %.cpp,%.o,$(SOURCES))
LIB_OBJS = $(patsubst %.cpp,%.o,$(LIB_SOURCES))

prog = programme
progs = $(prog)-shared $(prog)-static
lib_name = pg_affichage
lib_static = lib$(lib_name).a
lib_shared = lib$(lib_name).so

all: $(progs)

# Voir
# https://www.gnu.org/software/make/manual/make.html#Pattern-Match
%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $^

$(lib_static): $(LIB_OBJS)
	$(AR) rcs $@ $^

$(lib_shared): $(LIB_OBJS)
	$(CXX) -g -shared -Wl,-soname=$@.0 -o $@.0.0 $^ $(LDFLAGS)
	ln -sf $@.0.0 $@.0
	ln -sf $@.0.0 $@

$(prog)-static: $(OBJS) $(lib_static)
	$(CXX) -g -o $@ $^ $(LDFLAGS)

# Voir https://ftp.gnu.org/old-gnu/Manuals/ld-2.9.1/html_mono/ld.html
# pour l'explication du 'rpath'
#
# -rpath dir
#
#		 Add a directory to the runtime library search path. This is used
#    when linking an ELF executable with shared objects. All -rpath
#    arguments are concatenated and passed to the runtime linker,
#    which uses them to locate shared objects at runtime. The -rpath
#    option is also used when locating shared objects which are needed
#    by shared objects explicitly included in the link; see the
#    description of the -rpath-link option. If -rpath is not used when
#    linking an ELF executable, the contents of the environment
#    variable LD_RUN_PATH will be used if it is defined. The -rpath
#    option may also be used on SunOS. By default, on SunOS, the
#    linker will form a runtime search patch out of all the -L options
#    it is given. If a -rpath option is used, the runtime search path
#    will be formed exclusively using the -rpath options, ignoring the
#    -L options. This can be useful when using gcc, which adds many -L
#    options which may be on NFS mounted filesystems. For
#    compatibility with other ELF linkers, if the -R option is
#    followed by a directory name, rather than a file name, it is
#    treated as the -rpath option.
$(prog)-shared: $(OBJS) $(lib_shared)
	$(CXX) -o $@ $(OBJS) -Wl,-rpath=. $(LDFLAGS) -L. -l$(lib_name)

#make clean
.PHONY: clean
clean:
	rm -f $(OBJS) $(LIB_OBJS) $(lib_static) $(lib_shared) $(lib_shared).0.0 $(progs)
