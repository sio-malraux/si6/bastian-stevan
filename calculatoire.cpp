#include <libpq-fe.h>

#include <iostream>
#include <iomanip>
#include <cstring>

#include "affichage.h"
#include "calculatoire.h"

int calcul_longueur_champ_plus_long(PGresult *resultat_req)
{
  int longueur_champ_plus_long = 0;
  int nombre_champs = PQnfields(resultat_req);

  for(int compteur_champs = 0; compteur_champs < nombre_champs; compteur_champs++)
  {
    int longueur_champ = std::strlen(PQfname(resultat_req, compteur_champs));

    if(longueur_champ > longueur_champ_plus_long)
    {
      longueur_champ_plus_long = longueur_champ;
    }
  }

  return longueur_champ_plus_long;
}

