#include <libpq-fe.h>

#include <iostream>
#include <iomanip>
#include <cstring>

#include "affichage.h"
#include "calculatoire.h"


int main()
{
  PGPing code_retour_ping;
  PGconn *conn;
  code_retour_ping = PQping("host=postgresql.bts-malraux72.net port=5432");
  const char *requete =  "SELECT \"Animal\".\"id\" ,\"Animal\".\"nom\" AS \"nom de l'animal\", \"Animal\".\"sexe\", \"Animal\".\"date_naissance\" AS \"date de naissance\", \"Animal\".\"commentaires\", \"Race\".\"nom\" AS \"race\", \"Race\".\"description\" FROM \"si6\".\"Animal\" INNER JOIN \"si6\".\"Race\" ON \"Animal\".\"race_id\" = \"Race\".\"id\" WHERE \"Race\".\"nom\" = 'Singapura' and \"sexe\" = 'Femelle'";
  PGresult *resultat_req;
  const char *separateur = " | ";

  if(code_retour_ping == PQPING_OK)
  {
    std::cout << "La connexion au serveur de base de données a été établie avec les paramètres suivants : " << std::endl;
    const char *conninfo;
    conninfo = "host=postgresql.bts-malraux72.net port=5432 dbname=s.blanchard user=s.blanchard";
    conn = PQconnectdb(conninfo);

    if(PQstatus(conn) != CONNECTION_OK)
    {
      std::cerr << "Connexion failled" << std::endl;
      std::cout << PQerrorMessage(conn) << std::endl;
    }
    else
    {
      resultat_req = PQexec(conn, requete);
      //std::cout << "Connexion succes" << std::endl;
      affichage_information_connexion(conn);
      affichage_information_sur_requete(conn, resultat_req);
      int longueur_champ_plus_long = calcul_longueur_champ_plus_long(resultat_req);
      affichage(conn, resultat_req, separateur, longueur_champ_plus_long);
    }
  }
  else
  {
    std::cerr << "Malheureusement, le serveur n'est pas joignable, vérifier la connectivité" << std::endl;
  }

  return 0;
}

