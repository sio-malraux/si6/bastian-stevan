#ifndef _AFFICHAGE_H
  #define _AFFICHAGE_H

  #include <libpq-fe.h>
  #include <iostream>
  #include <iomanip>
  #include <cstring>

#endif

void affichage_information_connexion(PGconn *);
void affichage(PGconn *, PGresult *, const char *, int);
void affichage_information_sur_requete(PGconn *, PGresult *);
void affichage_ligne(PGresult *, int);
void affichage_entete(PGresult *, const char *, int);
void affichage_resultat_requete(PGresult *, const char *, int);
